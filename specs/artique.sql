CREATE SCHEMA artique;

CREATE TABLE contents (
	id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(255) NOT NULL,
    creation DATETIME NOT NULL,
    image VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE users (
	name VARCHAR(25) NOT NULL,
    password VARCHAR(255) NOT NULL,
    admin TINYINT(1) NOT NULL DEFAULT 0,
    PRIMARY KEY (name)
);

CREATE TABLE comments (
	id INT NOT NULL AUTO_INCREMENT,
	user VARCHAR(25) NOT NULL,
    content INT NOT NULL,
    text VARCHAR(255) NOT NULL,
    PRIMARY KEY (id, user, content),
	FOREIGN KEY (user) REFERENCES users (name)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	FOREIGN KEY (content) REFERENCES contents (id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

CREATE TABLE likes (
	user VARCHAR(25) NOT NULL,
    content INT NOT NULL,
    PRIMARY KEY (user, content),
	FOREIGN KEY (user) REFERENCES users (name)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	FOREIGN KEY (content) REFERENCES contents (id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

CREATE TABLE follows ( -- user 1 follows user 2, not viceversa
	follower VARCHAR(25) NOT NULL,
    followed VARCHAR(25) NOT NULL,
    PRIMARY KEY (follower, followed),
	FOREIGN KEY (follower) REFERENCES users (name)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	FOREIGN KEY (followed) REFERENCES users (name)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);
