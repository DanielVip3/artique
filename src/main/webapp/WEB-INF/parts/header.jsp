<% boolean logged = true; %>

<header>
    <div id="sidebar-overlay" class="hidden"></div>
    <div id="sidebar" class="hidden">
        <section class="close-container">
            <button id="close-sidebar-button">
                <object class="icon" data="icons/close-square-outline.svg"></object>
            </button>
        </section>
        <section class="logo-container">
            <object class="logo" data="icons/artique.svg"></object>
        </section>
        <section class="links-container">
            <% if (logged) { %>
                <a href="post"><object class="icon" data="icons/plus-circle-outline.svg"></object> Inserisci contenuto</a>
                <a href="profile"><object class="icon" data="icons/person.svg"></object> Profilo</a>
                <a href="logout"><object class="icon" data="icons/log-out-outline.svg"></object> Log-out</a>
            <% } else { %>
                <a href="login">Log-in <object class="icon" data="icons/log-in-outline.svg"></object></a>
            <% } %>
        </section>
    </div>
    <div id="horizontal">
        <section class="logo-container big-screen">
            <object class="logo" data="icons/artique.svg"></object>
        </section>
        <section class="small-screen">
            <button id="hamburger-button">
                <object class="icon" data="icons/menu-2.svg"></object>
            </button>
        </section>
        <section class="spacer"></section>
        <section>
            <input id="search-bar"
                   type="text"
                   placeholder="Cerca..."
                   style="background-image: url('icons/search.svg');"
            />
        </section>
        <% if (logged) { %>
            <section class="big-screen">
                <a id="post-content-link" href="post">
                    <object data="icons/plus-outline.svg"></object>
                </a>
            </section>
            <section class="big-screen">
                <div>
                    <button id="profile-button">
                        <object data="icons/person.svg"></object>
                    </button>
                </div>
                <div id="profile-dropdown" class="hidden">
                    <a href="profile">Profilo <object class="icon" data="icons/person-black.svg"></object></a>
                    <a href="logout">Log-out <object class="icon" data="icons/log-out-outline-black.svg"></object></a>
                </div>
            </section>
        <% } else { %>
            <section class="big-screen">
                <a id="login-link" href="login">Log-in <object class="icon" data="icons/log-in-outline.svg"></object></a>
            </section>
        <% } %>
    </div>
</header>