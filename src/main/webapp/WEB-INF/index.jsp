<jsp:useBean id="contents" scope="request" type="java.util.List"/>
<%@ taglib prefix="c" uri="jakarta.tags.core" %>
<%@ taglib prefix="h" tagdir="/WEB-INF/tags" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Artique</title>
        <link rel="stylesheet" type="text/css" href="css/header.css" />
        <link rel="stylesheet" type="text/css" href="css/content-tag.css" />
        <link rel="stylesheet" type="text/css" href="css/index.css" />
        <%@ include file="parts/head.jsp" %>
    </head>
    <body>
        <%@ include file="parts/header.jsp" %>

        <div id="page">
            <div id="page-content">
                <section class="row">
                    <c:forEach var="content" items="${contents}" varStatus="loop">
                        <c:if test="${loop.index % 3 == 0}">
                            <h:content content="${content}" />
                        </c:if>
                    </c:forEach>
                </section>

                <section class="row">
                    <c:forEach var="content" items="${contents}" varStatus="loop">
                        <c:if test="${loop.index % 3 == 1}">
                            <h:content content="${content}" />
                        </c:if>
                    </c:forEach>
                </section>

                <section class="row">
                    <c:forEach var="content" items="${contents}" varStatus="loop">
                        <c:if test="${loop.index % 3 == 2}">
                            <h:content content="${content}" />
                        </c:if>
                    </c:forEach>
                </section>
            </div>
        </div>

        <script src="js/content-tag.js"></script>
        <script src="js/header.js"></script>
    </body>
</html>