<jsp:useBean id="Content" class="it.unisa.artique.model.Content" scope="page" />
<%@ attribute name="content" required="true" type="it.unisa.artique.model.Content" %>

<article class="content">
    <figure>
        <img src="${content.image}"  alt="${content.title}"/>

        <figcaption class="hidden overlay">
            ${content.title}
        </figcaption>
    </figure>
</article>
