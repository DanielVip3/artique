document.addEventListener("DOMContentLoaded", function(event) {
    const overlay = document.getElementById("sidebar-overlay");
    const sidebar = document.getElementById("sidebar");
    const closeButton = document.getElementById("close-sidebar-button");
    const profileButton = document.getElementById("profile-button");
    const profileDropdown = document.getElementById("profile-dropdown");

    let sidebarOpened = false;
    document.getElementById("hamburger-button").addEventListener("click", function(event) {
        if (sidebarOpened) {
            overlay.classList.add("hidden");
            sidebar.classList.add("hidden");
        } else {
            overlay.classList.remove("hidden");
            sidebar.classList.remove("hidden");
        }

        sidebarOpened = !sidebarOpened;
    });

    overlay.addEventListener("click", function(event) {
        overlay.classList.add("hidden");
        sidebar.classList.add("hidden");

        sidebarOpened = false;
    });

    closeButton.addEventListener("click", function(event) {
        overlay.classList.add("hidden");
        sidebar.classList.add("hidden");

        sidebarOpened = false;
    });

    let profileDropdownOpened = false;
    profileButton.addEventListener("click", function(event) {
        if (profileDropdownOpened) {
            profileDropdown.classList.add("hidden");
        } else {
            profileDropdown.classList.remove("hidden");
        }

        profileDropdownOpened = !profileDropdownOpened;
    });

    document.addEventListener("click", function(event) {
        console.log(event.target);
        if (event.target !== profileDropdown && event.target !== profileButton && !profileDropdown.contains(event.target)) {
            if (profileDropdownOpened) {
                profileDropdown.classList.add("hidden");

                profileDropdownOpened = false;
            }
        }
    });
});