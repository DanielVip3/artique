document.addEventListener("DOMContentLoaded", function(event) {
    let showing = null;
    document.addEventListener("mouseover", function(event) {
        const content = event.target.closest(".content");
        if (content) {
            const overlay = content.querySelector(".overlay");
            if (showing != null && overlay !== showing) showing.classList.add("hidden");

            overlay.classList.remove("hidden");
            showing = overlay;
        } else {
            if (showing != null) showing.classList.add("hidden");
            showing = null;
        }
    });
});