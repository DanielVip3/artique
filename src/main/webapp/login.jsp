
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Artique</title>
        <link rel="stylesheet" type="text/css" href="css/header.css" />
        <link rel="stylesheet" type="text/css" href="css/login.css">

        <%@ include file="WEB-INF/parts/head.jsp" %>
    </head>


    <body>
        <%@ include file="WEB-INF/parts/header.jsp" %>
        <div class="flex-container">

            <form name=form action="login-servlet" method = "post">
                <label for="Nome">Nome Utente: </label>
                <input type="text" id="Nome" name="nome" required>
                <label for="Password">Password:</label>
                <input type="password" id="Password" name="password">

                <div>
                    <input type="checkbox" id="check" name="check" value="check">
                    <label for="check">Rimani connesso</label>
                </div>

                <label for="SubmitButton"></label>
                <input type="submit" class="submit-button" id="SubmitButton" name="SubBut" value="Accedi">
            </form>
        </div>

        <script src="js/header.js"></script>
    </body>
</html>
