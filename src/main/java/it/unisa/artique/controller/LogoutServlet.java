package it.unisa.artique.controller;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

@WebServlet("/logout-servlet")
public class LogoutServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession userSess = request.getSession();

        if (userSess != null) userSess.invalidate();

        response.sendRedirect("http://localhost:8080/artique_war_exploded/copiaindex.jsp");
    }
}
