package it.unisa.artique.controller;

import java.io.*;

import it.unisa.artique.model.User;
import it.unisa.artique.model.UserDAO;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;

@WebServlet("/registration-servlet")
public class RegistrationServlet extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        HttpSession userSess = request.getSession(true);
        String name = request.getParameter("name");
        String password = request.getParameter("password");

        User user = new User();
        user.setName(name);
        user.setPassword(password);
        user.setAdmin(false);

        UserDAO userDAO = new UserDAO();
        userDAO.doSave(user);

        userSess.setAttribute("user", user);
    }
}