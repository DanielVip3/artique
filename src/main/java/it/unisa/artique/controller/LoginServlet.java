package it.unisa.artique.controller;

import it.unisa.artique.model.User;
import it.unisa.artique.model.UserDAO;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

@WebServlet("/login-servlet")
public class LoginServlet extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession userSess = request.getSession(true);
        String name = request.getParameter("name");
        String password = request.getParameter("password");

        String address = "/index.jsp";

        UserDAO userDAO = new UserDAO();
        User user = userDAO.find(name, password);

        if (user != null) {
            userSess.setAttribute("user", user);
        } else {
            address = "/error.html";
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher(address);
        dispatcher.forward(request, response);
    }
}