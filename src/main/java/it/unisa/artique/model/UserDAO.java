package it.unisa.artique.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAO {
    public void doSave(User user) {
        try (Connection con = Database.getConnection()) {
            PreparedStatement ps = con.prepareStatement("INSERT INTO users (name, password, admin) VALUES(?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, user.getName());
            ps.setString(2, user.getPassword());
            ps.setDouble(3, 0);

            if (ps.executeUpdate() != 1) {
                throw new RuntimeException("INSERT error.");
            }

            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public User find(String name, String password) {
        try (Connection con = Database.getConnection()) {
            PreparedStatement ps = con.prepareStatement("SELECT name, password, admin FROM users WHERE name = ? AND password = ?");
            ps.setString(1, name);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                User user = new User();
                user.setName(name);
                user.setPassword(password);
                user.setAdmin(rs.getBoolean("admin"));

                return user;
            } else return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
