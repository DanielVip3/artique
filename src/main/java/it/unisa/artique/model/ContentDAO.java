package it.unisa.artique.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;

public class ContentDAO {
    public List<Content> findAll() {
        List<Content> contents = new ArrayList<>();

        try (Connection con = Database.getConnection()) {
            PreparedStatement ps = con.prepareStatement("SELECT * FROM contents");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Content content = new Content();
                content.setId(rs.getInt("id"));
                content.setTitle(rs.getString("title"));
                content.setCreation(rs.getTimestamp("creation").toLocalDateTime());
                content.setImage(rs.getString("image"));

                contents.add(content);
            }

            return contents;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
